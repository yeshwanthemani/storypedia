from facebookads.api import FacebookAdsApi
from facebookads import objects
from facebookads.adobjects.campaign import Campaign

class FacebookAdsWrapper(object):
   
    def __init__(self,app_id,app_secret,app_token):
        self.app_id = app_id
        self.app_secret = app_secret
        self.app_token = app_token

        FacebookAdsApi.init(app_id,app_secret,app_token)
        self.me = objects.AdUser(fbid='me')
        self.my_account = self.me.get_ad_accounts()[0]

    def createCampaign(self,name,objective):
        campaign = objects.Campaign(parent_id = self.my_account.get_id_assured())
        campaign[objects.Campaign.Field.name] = name 
        campaign[objects.Campaign.Field.objective] = objective
        campaign[objects.Campaign.Field.configured_status] = objects.Campaign.Status.paused
        campaign.remote_create()
        return "Successfully Created Campaign"
    
    def getCampaign(self,campaign_id):
        campaign = objects.Campaign(campaign_id)
        campaign.remote_read(fields=[
        Campaign.Field.name,
        Campaign.Field.effective_status,
        Campaign.Field.objective,
        ])
        return campaign
    

    def updateCampaign(self,campaign_id,name = None,objective = None, configured_status = None):
        campaign = objects.Campaign(campaign_id)
        if name != None:
            campaign[objects.Campaign.Field.name] = name
            print "Updated Name to" + name
        if objective != None:
            campaign[objects.Campaign.Field.objective] = objective
            print "Updated Objective"
        if configured_status != None:
            campaign[objects.Campaign.Field.configured_status] = configured_status
            print "Updated Configured_status"
        campaign.remote_update()    
        return "Successfully Updated Campaign"    

    def deleteCampaign(self,campaign_id):
        campaign = objects.Campaign(campaign_id)
        campaign.remote_delete()
        return "Successfully Deleted Campaign"
        
    
    
        
        
