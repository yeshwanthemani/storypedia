import json
import urllib2

def get_page_data(page_id,access_token):
    api_url = "https://graph.facebook.com/v2.4/"
    requrl = api_url + page_id +"?fields=id,name,likes,link&access_token="+access_token

    try:
      
        api_request = urllib2.Request(requrl)
        res = urllib2.urlopen(api_request)
        try:
            return json.loads(res.read())
        except (ValueError, KeyError, TypeError):
            return "JSON error"
    except IOError, e:
        if hasattr(e, 'code'):
            return e.code
        elif hasattr(e, 'reason'):
            return e.reason

x = "218954951630952"
y = "EAACEdEose0cBAIXopMpTZCjcmd2fnoiN8F7pWpInEcAIZAq6tDLiGKkWLPLX2HiTSg28FszhR7HO3H19PckMQjEtZAEdtXrnOQMQYToJNn0D8m2xPKPn1AZCD1xaTP2ee3UIw1CokkXG4O0auZA8bUVvWjc7Ndo9m079QDp5ofwZDZD"
page_data = get_page_data(x,y)

print page_data
